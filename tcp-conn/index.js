const net = require('net');
const EventEmitter = require('events').EventEmitter;

class Socket extends EventEmitter {
  constructor() {
    super();

    this.socket = undefined;
    this.host = '';
    this.port = 0;
    this.forceDisconnect = false;
    this.options = {
      retries1: 0,
      time1: 2000,
      retries2: 0,
      time2: 5000,
      lastRetry: 0,
      currentPhase: 1,
    };
    this.reconnectTimer = undefined;
  }

  setAutoReconnect(retries1, time1, retries2, time2) {
    this.options = {
      retries1,
      time1,
      retries2,
      time2,
      lastRetry: 0,
      currentPhase: 1,
    };
  }

  connect(port, host) {
    this.port = port;
    this.host = host;

    this.forceDisconnect = false;
    this.options.lastRetry = 0;
    this.options.currentPhase = 1;

    this.internalConnect();
  }

  destroy() {
    if (this.socket) {
      this.forceDisconnect = true;
      this.socket.destroy();
      this.socket = undefined;
    }
  }

  write(data) {
    if (this.socket && data) {
      this.socket.write(data);
    }
  }

  internalConnect() {
    if (this.reconnectTimer) {
      clearTimeout(this.reconnectTimer);
      this.reconnectTimer = undefined;
    }
    if (this.socket) {
      this.socket.removeAllListeners();
      this.socket.destroy();
    }
    this.socket = new net.Socket();

    this.socket.on('close', this.onClose.bind(this));
    this.socket.on('connect', this.onConnect.bind(this));
    this.socket.on('data', this.onData.bind(this));
    this.socket.on('drain', this.onDrain.bind(this));
    this.socket.on('end', this.onEnd.bind(this));
    this.socket.on('error', this.onError.bind(this));
    this.socket.on('lookup', this.onLookup.bind(this));
    this.socket.on('timeout', this.onTimeout.bind(this));

    this.socket.connect(this.port, this.ip);

    if (this.options.lastRetry > 0) {
      this.emit(
        'connecting',
        this.options.currentPhase,
        this.options.currentPhase <= 1 ? this.options.lastRetry : (this.options.lastRetry - this.options.retries1)
      );
    }
  }


  manageConnectionLoss() {
    this.options.lastRetry += 1;

    const retry = this.options.lastRetry;
    const retries1 = this.options.retries1;
    const retries2 = this.options.retries2;
    const time1 = this.options.time1;
    const time2 = this.options.time2;

    let wait = 0;

    if (retry <= retries1) {
      wait = time1;
    } else if ((retries2 === 0) || (retry > (retries1 + retries2))) {
      this.options.currentPhase = 2;
      wait = time2;
    }

    if (wait) {
      this.reconnectTimer = setTimeout(this.internalConnect.bind(this), wait);
    }
  }


  onClose(hadError) {
    if (this.forceDisconnect) {
      this.forceDisconnect = false;
      this.emit('close', hadError);
    } else {
      if (this.options.lastRetry === 0) {
        this.emit('disconnect');
      }
      this.manageConnectionLoss();
    }
  }

  onConnect() {
    this.options.lastRetry = 0;
    this.options.currentPhase = 1;

    this.emit('connect');
  }

  onData(buffer) {
    this.emit('data', buffer);
  }

  onDrain() {
    this.emit('drain');
  }

  onEnd() {
    this.emit('end');
  }

  onError(err) {
    this.emit('error', err);
  }

  onLookup(err, address, family, host) {
    this.emit('lookup', err, address, family, host);
  }

  onTimeout() {
    this.emit('timeout');
  }

}

module.exports = Socket;
