const net = require('net');

//
// Constants and defaults
//

const DEFAULT_DEBUG_ENABLED = false;
const DEFAULT_EVENT_PREFIX = '_socket_';

const DEFAULT_RECONNECT_N1 = 5;
const DEFAULT_RECONNECT_T1 = 2000;
const DEFAULT_RECONNECT_N2 = 0;
const DEFAULT_RECONNECT_T2 = 10000;

//
// Helper functions
//

// Get the caller class for a method
function getCallerClass() {
  const stackTraceParent = ((new Error().stack).split('at ')[3]).trim();
  const callerClass = stackTraceParent.split('.')[0].split(' ')[0];

  return callerClass;
}

//
// Socket class
//
class Socket extends net.Socket {
  constructor(options) {
    super(options);

    // Default values
    this.debugEnabled = DEFAULT_DEBUG_ENABLED;
    this.eventPrefix = DEFAULT_EVENT_PREFIX;

    // Base socket listeners
    super.on('close', this.onClose.bind(this));
    super.on('connect', this.onConnect.bind(this));
    super.on('data', this.onData.bind(this));
    super.on('drain', this.onDrain.bind(this));
    super.on('end', this.onEnd.bind(this));
    super.on('error', this.onError.bind(this));
    super.on('lookup', this.onLookup.bind(this));
    super.on('timeout', this.onTimeout.bind(this));

    // Base socket configuration
    super.setKeepAlive(1000);
    super.setNoDelay();

    // Socket properties
    this.connected = false;
    this.connectArgs = undefined;
    this.forcedDisconnect = false;
    this.reconnectTimer = undefined;

    this.reconnect = {
      enabled: true,
      n1: DEFAULT_RECONNECT_N1,
      t1: DEFAULT_RECONNECT_T1,
      n2: DEFAULT_RECONNECT_N2,
      t2: DEFAULT_RECONNECT_T2,
      phase: 1,
      retry: 0,
    };
  }

  //
  // Helper methods
  //

  // Get a prefixed event name
  evt(eventName) {
    return `${this.eventPrefix}${eventName}`;
  }

  // Show debug messages
  debugMsg(msg) {
    if (this.debugEnabled) {
      console.log(msg);   // eslint-disable-line no-console
    }
  }

  // Set auto-reconnect settings
  setAutoReconnect(
    enabled = true,
    n1 = DEFAULT_RECONNECT_N1,
    t1 = DEFAULT_RECONNECT_T1,
    n2 = DEFAULT_RECONNECT_N2,
    t2 = DEFAULT_RECONNECT_T2) {
    this.debugMsg('[SOCKET.setAutoReconnect]');
    this.reconnect = {
      enabled,
      n1,
      t1,
      n2,
      t2,
      phase: this.reconnect.phase,
      retry: this.reconnect.retry,
    };
  }

  // Activate / deactivate debug messages
  setDebug(debug) {
    if (debug !== undefined) {
      this.debugEnabled = debug;
    }
  }

  //
  // Connection methods
  //

  // Socket.connect() override
  connect(...args) {
    this.debugMsg('[SOCKET.connect]');
    if (this.connecting || this.connected) {
      return;
    }

    this.connectArgs = args;

    this.forcedDisconnect = false;
    this.reconnect.phase = 1;
    this.reconnect.retry = 0;

    this.internalConnect();
  }

  // Socket.destroy() override
  destroy(exception) {
    this.debugMsg('[SOCKET.destroy]');
    this.connected = false;
    super.destroy(exception);
  }

  // Internal connect method
  internalConnect() {
    this.debugMsg('[SOCKET.internalConnect]');
    if (this.reconnectTimer) {
      clearTimeout(this.reconnectTimer);
      this.reconnectTimer = undefined;
    }

    super.connect(...this.connectArgs);

    if (this.reconnect.retry > 0) {
      this.emit(this.evt('connecting'), this.reconnect.phase, this.reconnect.retry);
    }
  }

  // Connection loss management
  manageConnectionLoss() {
    this.debugMsg('[SOCKET.manageConnectionLoss]');
    if (!this.reconnect.enabled) {
      return;
    }

    this.reconnect.retry += 1;

    let wait = 0;

    if (this.reconnect.phase === 1) {
      wait = this.reconnect.t1;

      if ((this.reconnect.n1 === 0) || (this.reconnect.retry > this.reconnect.n1)) {
        this.reconnect.phase = 2;
        this.reconnect.retry = 1;
      }
    }

    if (this.reconnect.phase === 2) {
      wait = this.reconnect.t2;

      if ((this.reconnect.n2 > 0) && (this.reconnect.retry > this.reconnect.n2)) {
        // STOP reconnection
        return;
      }
    }

    if (wait) {
      this.reconnectTimer = setTimeout(this.internalConnect.bind(this), wait);
    }
  }

  //
  // Socket event handlers
  //

  onClose(hadError) {
    this.debugMsg('[SOCKET.onClose]');
    this.connected = false;

    if (this.forcedDisconnect) {
      this.forcedDisconnect = false;
      this.emit(this.evt('close'), hadError);
    } else {
      if (this.reconnect.retry === 0) {
        this.emit(this.evt('disconnect'));
      }
      this.manageConnectionLoss();
    }
  }

  onConnect() {
    this.debugMsg('[SOCKET.onConnect]');
    this.connected = true;
    this.reconnect.phase = 1;
    this.reconnect.retry = 0;

    this.emit(this.evt('connect'));
  }

  onData(buffer) {
    this.debugMsg('[SOCKET.onData]');
    this.emit(this.evt('data'), buffer);
  }

  onDrain() {
    this.debugMsg('[SOCKET.onDrain]');
    this.emit(this.evt('drain'));
  }

  onEnd() {
    this.debugMsg('[SOCKET.onEnd]');
    this.connected = false;
    this.emit(this.evt('end'));
  }

  onError(err) {
    this.debugMsg('[SOCKET.onError]');
    this.connected = false;
    this.emit(this.evt('error'), err);
  }

  onLookup(err, address, family, host) {
    this.debugMsg('[SOCKET.onLookup]');
    this.emit(this.evt('lookup'), err, address, family, host);
  }

  onTimeout() {
    this.debugMsg('[SOCKET.onTimeout]');
    this.connected = false;
    this.emit(this.evt('timeout'));
  }

  //
  // Overriden EventEmitter methods
  //

  addListener(eventName, listener) {
    this.debugMsg(`[SOCKET.addListener] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.on(eventName, listener);
    }

    return this.on(this.evt(eventName), listener);
  }

  on(eventName, listener) {
    this.debugMsg(`[SOCKET.on] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.on(eventName, listener);
    }

    return this.on(this.evt(eventName), listener);
  }

  once(eventName, listener) {
    this.debugMsg(`[SOCKET.once] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.once(eventName, listener);
    }

    return this.once(this.evt(eventName), listener);
  }

  listenerCount(eventName) {
    this.debugMsg(`[SOCKET.listenerCount] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.listenerCount(eventName);
    }

    return this.listenerCount(this.evt(eventName));
  }

  listeners(eventName) {
    this.debugMsg(`[SOCKET.listeners] (${eventName})`);

    if (!eventName) {
      return super.listeners();
    }

    if (getCallerClass() === 'Socket') {
      return super.listeners(eventName);
    }

    return this.listeners(this.evt(eventName));
  }

  prependListener(eventName, listener) {
    this.debugMsg(`[SOCKET.prependListener] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.prependListener(eventName, listener);
    }

    return this.prependListener(this.evt(eventName), listener);
  }

  prependOnceListener(eventName, listener) {
    this.debugMsg(`[SOCKET.prependOnceListener] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.prependOnceListener(eventName, listener);
    }

    return this.prependOnceListener(this.evt(eventName), listener);
  }

  removeAllListeners(eventName) {
    this.debugMsg(`[SOCKET.removeAllListeners] (${eventName})`);
    if (eventName) {
      if (getCallerClass() === 'Socket') {
        return super.removeAllListeners(eventName);
      }
      return this.removeAllListeners(this.evt(eventName));
    }

    let retVal = undefined;

    /* eslint-disable no-restricted-syntax */
    for (const evName in this._events) {
      if (evName.substring(0, this.eventPrefix.length) === this.eventPrefix) {
        retVal = this.removeAllListeners(evName);
      }
    }
    /* eslint-enable no-restricted-syntax */

    return retVal;
  }

  removeListener(eventName, listener) {
    this.debugMsg(`[SOCKET.removeListener] (${eventName})`);

    if (getCallerClass() === 'Socket') {
      return super.removeListener(eventName, listener);
    }

    return this.removeListener(this.evt(eventName), listener);
  }
}

module.exports = Socket;
