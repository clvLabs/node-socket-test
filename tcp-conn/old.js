const net = require('net');
const EventEmitter = require('events').EventEmitter;

class TCPConn extends EventEmitter {
  constructor() {
    super();
    this.socket = undefined;
    this.ip = '';
    this.port = 0;
    this.forceDisconnect = false;
    this.options = {
      retries1: 0,
      time1: 2000,
      retries2: 0,
      time2: 5000,
      lastRetry: 0,
      currentPhase: 1,
    };
    this.reconnectTimer = undefined;
  }

  setAutoReconnect(retries1, time1, retries2, time2) {
    this.options = {
      retries1,
      time1,
      retries2,
      time2,
      lastRetry: 0,
      currentPhase: 0,
    };
  }

  connect(ip, port) {
    this.ip = ip;
    this.port = port;

    this.options.lastRetry = 0;
    this.options.currentPhase = 1;

    this._connect();
  }

  disconnect() {
    if (this.socket) {
      this.forceDisconnect = true;
      this.socket.destroy();
      this.socket = undefined;
    }
  }

  send(data) {
    if (this.socket && data) {
      this.socket.write(data);
    }
  }

  _connect() {
    if (this.reconnectTimer) {
      clearTimeout(this.reconnectTimer);
      this.reconnectTimer = undefined;
    }
    if (this.socket) {
      this.socket.removeAllListeners();
      this.socket.destroy();
    }
    this.socket = new net.Socket();

    this.socket.on('error', this._onError.bind(this));
    this.socket.on('data', this._onData.bind(this));
    this.socket.on('close', this._onClose.bind(this));

    this.socket.connect(this.port, this.ip, this._onConnect.bind(this));

    if (this.options.lastRetry > 0) {
      this.emit(
        'connecting',
        this.options.currentPhase,
        this.options.currentPhase <= 1 ? this.options.lastRetry : (this.options.lastRetry - this.options.retries1)
      );
    }
  }

  _manageConnectionLoss() {
    this.options.lastRetry++;

    const retry = this.options.lastRetry;
    const retries1 = this.options.retries1;
    const retries2 = this.options.retries2;
    const time1 = this.options.time1;
    const time2 = this.options.time2;

    let wait = 0;

    if (retry <= retries1) {
      wait = time1;
    } else {
      if ((retries2 === 0) || (retry > (retries1 + retries2))) {
        this.options.currentPhase = 2;
        wait = time2;
      }
    }

    if (wait) {
      this.reconnectTimer = setTimeout(this._connect.bind(this), wait);
    }
  }

  _onConnect() {
    this.options.lastRetry = 0;
    this.options.currentPhase = 1;

    this.emit('connect');
  }

  _onError(err) {
    this.emit('error', err);
  }

  _onData(data) {
    this.emit('receive', data);
  }

  _onClose() {
    if (this.forceDisconnect) {
      this.forceDisconnect = false;
      this.emit('close');
    } else {
      if (this.options.lastRetry === 0) {
        this.emit('disconnect');
      }
      this._manageConnectionLoss();
    }
  }

}

module.exports = TCPConn;
