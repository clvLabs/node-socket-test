const moment = require('moment');

function showLog(text) {
  console.log(`[${moment().format('h:mm:ss.SSS')}] ${text}`);
}


module.exports = {
  showLog,
};
