const net = require('net');
const settings = require('./settings.js');
const utils = require('./utils.js');

const clients = [];

// Send a message to all clients
function broadcast(message, sender) {
  utils.showLog(`[broadcast] [${message}] clients: (${clients.length})`);
  clients.forEach((client) => {
    if (client !== sender) {
      utils.showLog(`  - [client] (${client.name})`);
      client.write(message);
    }
  });
}

function handleNewClient(socket) {
  utils.showLog(`-----------------------------------[ ${socket.remoteAddress}:${socket.remotePort} JOINED`);

  // Handle errors
  socket.on('error', (err) => {
    utils.showLog(`[onError] ${socket.name}> ${err.code}`);
    clients.splice(clients.indexOf(socket), 1);
    broadcast(`${socket.name} left the chat.`);
    socket.destroy();
  });

  // Handle incoming messages from clients.
  socket.on('data', (data) => {
    utils.showLog(`[onData] ${socket.name}> ${data}`);
    broadcast(`${socket.name}> ${data}`, socket);
  });

  // Remove the client from the list when it leaves
  socket.on('end', () => {
    utils.showLog(`[onEnd] ${socket.name}>`);
    clients.splice(clients.indexOf(socket), 1);
    broadcast(`${socket.name} left the chat.`);
  });

  // Identify this client
  socket.name = `${socket.remoteAddress}:${socket.remotePort}`; // eslint-disable-line no-param-reassign

  // Put this new client in the list
  clients.push(socket);

  // Send a nice welcome message and announce
  utils.showLog(`[send] (Welcome ${socket.name})`);
  socket.write(`Welcome ${socket.name}`);
  broadcast(`${socket.name} joined the chat`, socket);
}

const server = net.createServer(handleNewClient);

server.on('error', (err) => {
  utils.showLog(`[SERVER][onError]  ${err.code}`);
});

if (process.argv.length > 2) { settings.port = process.argv[2]; }
if (process.argv.length > 3) { settings.host = process.argv[3]; }

utils.showLog(`Server listening on ${settings.host}:${settings.port}`);
server.listen(settings.port, settings.host);
