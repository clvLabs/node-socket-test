const Socket = require('./tcp-conn/socket.js');
const settings = require('./settings.js');
const utils = require('./utils.js');

//
// Socket object
//
const client = new Socket();
let cliAddr = '[notSet]';

//
// "Hi" timer methods
//
function sayHi() {
  if (client.connected) {
    utils.showLog(`[${cliAddr}] [send] hi`);
    client.write('hi');
  } else {
    utils.showLog(`[${cliAddr}] [send] CANCELLED (disconnected...)`);
  }
}

let sayHiTimer = undefined; // eslint-disable-line no-undef-init

function stopSayHiTimer() {
  if (sayHiTimer) {
    clearInterval(sayHiTimer);
    sayHiTimer = undefined;
  }
}

function startSayHiTimer() {
  stopSayHiTimer();
  sayHiTimer = setInterval(sayHi, 15000);
}

//
// Socket event listeners
//
client.on('close', () => {
  utils.showLog(`[${cliAddr}] [onClose]`);
  stopSayHiTimer();
  client.destroy();
});

client.on('connect', () => {
  utils.showLog(`[${cliAddr}] [onConnect] local: ${client.localAddress}:${client.localPort}`);
  utils.showLog(`[${cliAddr}] [send] Hello, server!`);
  client.write('Hello, server!');
  startSayHiTimer();
});

client.on('connecting', (phase, retry) => {
  utils.showLog(`[${cliAddr}] [onConnecting] ${phase}:${retry}`);
});

client.on('data', (data) => {
  utils.showLog(`[${cliAddr}] [onData] ${data}`);
});

client.on('disconnect', () => {
  utils.showLog(`[${cliAddr}] [onDisconnect]`);
});

client.on('drain', () => {
  utils.showLog(`[${cliAddr}] [onDrain]`);
});

client.on('end', () => {
  utils.showLog(`[${cliAddr}] [onEnd]`);
});

client.on('error', (err) => {
  utils.showLog(`[${cliAddr}] [onError] ${err.code}`);
  stopSayHiTimer();
});

client.on('lookup', (err, address, family, host) => {
  utils.showLog(`[${cliAddr}] [onLookup] ${err} ${address} ${family} ${host}`);
});

client.on('timeout', () => {
  utils.showLog(`[${cliAddr}] [onTimeout]`);
});

//
// Socket configuration
//

client.setAutoReconnect(true);
// client.setAutoReconnect(true, 5, 100, 5, 3000);

let SOCKET_DEBUG = false;

if (process.argv.length > 2) { settings.port = process.argv[2]; }
if (process.argv.length > 3) { settings.host = process.argv[3]; }
if (process.argv.length > 4) { SOCKET_DEBUG = process.argv[4]; }

cliAddr = `${settings.host}:${settings.port}`;

//
// Socket connection
//
if (SOCKET_DEBUG) {
  utils.showLog('*** SOCKET DEBUG ACTIVE ***');
  client.setDebug(true);
}

utils.showLog(`[${cliAddr}] Connecting`);
client.connect(settings.port, settings.host);
